# 自动柯里化宏

自动完成对函数的柯里化

## 安装

```cj
[dependencies]
  cj_curry = { git = 'https://gitcode.com/unravel/cj_curry_macro.git', output-type='static', branch = 'main'}
```

## 使用

```cj
import cj_curry.macros.Curry

@Curry
func myFuncForCurry(a: Int, b: Int, c: Int) {
    a + b + c
}

main(): Unit {
    let a = myFuncForCurry(1)(2)(3)
    println(a)
    let b = myFuncForCurry(1, 2)(3)
    println(b)
    let c = myFuncForCurry(1, 2, 3)
    println(c)
}
```
