### 使用仓颉编写的分词器
- 支持编码和解码
- 支持直接读取Huggingface的tokenizer.json文件，仅测试qwen2系列，其它模型可能需要小小的魔改。
- 参考了mnn-llm的tokenizer.cpp文件，[链接](https://github.com/wangzhaode/mnn-llm/blob/master/src/tokenizer.cpp)
- 参考了CLIP项目的simple_tokenizer.py文件，[链接](https://github.com/openai/CLIP/blob/main/clip/simple_tokenizer.py)
- 参考了rust原版tokenizer项目，[链接](https://github.com/huggingface/tokenizers)

### 使用方法
1. 去Huggingface下载Qwen2系列的模型，例如`QWen2-0.5B-Instruct`，放到download文件夹，[下载地址](https://huggingface.co/Qwen/Qwen2-0.5B-Instruct)，[国内镜像下载地址](https://hf-mirror.com/Qwen/Qwen2-0.5B-Instruct)

2. 运行`cjpm run`，输出下面的信息则代表运行成功。
  ```bash
  ===== test1 ===== 
  encode_tokens: [99489, 108386, 3837, 14990, 1879, 0]
  deocode_str: 世界你好，hello world!
  ===== ===== =====
  ===== test2 =====
  special token: <|im_end|>
  encode_tokens: [99489, 108386, 3837, 14990, 1879, 0, 151645]
  deocode_str(with special)    : 世界你好，hello world!<|im_end|>
  deocode_str(without special) : 世界你好，hello world!
  ===== ===== =====
  ===== test3 =====
  new_text:
   <|im_start|>system
  You are a helpful assistant.<|im_end|>
  <|im_start|>user
  世界你好，hello world!<|im_end|>
  <|im_start|>assistant
  ```