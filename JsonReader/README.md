# SimpleJson

示例项目^_^

### 示例介绍
    在客户端与服务端交互的过程中，大多使用JSON来传递信息，本项目旨在让客户端更加便利的获取服务端返回的信息。

### 示例
```cangjie
let response = #"
{
    "data": {
        info: {
            "name": "Arror"
        },
        "other": {
            "key": "value"
        }
    }
    
}"#
println(Json(str: response)["data"]["info"]["name"].stringValue) // Arror
```